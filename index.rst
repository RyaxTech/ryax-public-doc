
.. meta::
   :description: Ryax: create quickly automations and APIs in the cloud.
.. meta::
   :property="og:description": Create quickly automations and APIs in the cloud.
.. meta::
   :property="og:title": Ryax Technologies.
.. meta::
   :property="og:url": https://docs.ryax.tech/index.html
.. meta::
   :property="og:image": https://docs.ryax.tech/_static/ryax-icon.png

Ryax Documentation
==================


Ryax is an open-source platform that streamlines the design, deployment, and monitoring of your Cloud automations and APIs. 

Ryax makes software projects more accessible to all types of developers, and reduces the amount of code needed to launch a software project. This is achieved by eliminating much of the boiler plate code needed across almost all software projects, and by allowing users an easy way to reuse the code they develop for their applications.

This is the best place to start with Ryax. In the following sections we cover what Ryax is, what problems it can solve, and how it compares to existing software.




Need a quick crash course? Jump right in our `Quick Start Guide <./tutorials/quick_start_guide.html>`_




What is Ryax?
-------------

.. image:: ./_static/web_interface/ryax-screenshot-execution.png


.. Seprate docs in 4: tutorials (to learn), how-to guides (for precise tasks), references (to describe the machinery), explanation (to understand).

.. toctree::
   :maxdepth: 2
   :caption: Concepts

   ./concepts/concepts
   ./concepts/comparisons
   ./concepts/use_cases


.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   ./tutorials/quick_start_guide


.. toctree::
   :maxdepth: 2
   :caption: How-to guides

   ./howto/install_ryax_kubernetes

.. toctree::
   :maxdepth: 2
   :caption: Reference

   ./reference/action_python3
   ./concepts/projects
   ./reference/architecture
   ./reference/glossary
   ./reference/basic_actions
