# Install Ryax on Kubernetes

We assume that you are comfortable with Kubernetes and therefore we do not provide too many details on the Kubernetes parts of the installation.

## Preparatory steps

- Make sure you're on the intended cluster: `kubectl config current-context`.
- Make sure the used namespaces are empty: we offer no guarantee that Ryax runs smoothly alongside other applications.
- Make sure you have complete admin access to the cluster. Try to run `kubectl auth can-i create ns` or `kubectl auth can-i create pc`, for instance.


## Customize your installation

Installing Ryax is analogous to installing a Helm chart. To begin we will start with a default configuration, and make a few tweaks so that everything is compatible with your Kubernetes provider. Be assured however that you will be able to fine-tune your installation later on.

Just like helm charts, we call "values" the configuration of a Ryax cluster. Let's initialize a basic values file.

```bash
docker run -v $PWD:/data/volume -u $UID ryaxtech/ryax-adm:latest init --values volume/ryax_values.yaml
vim ryax_values.yaml # Or your favorite text editor
```

The `clusterName` value is the name you give to your cluster, which is used in various places. One of those places is the URL of your cluster that will end up being \<clusterName\>.\<domainName\>.io, therefore it has to be consistent with your DNS.

The detail of all the values can be found in [ryax-adm/helm-charts/values.yaml](https://gitlab.com/ryax-tech/ryax/ryax-adm/-/blob/master/helm-charts/values.yaml), feel free to make them suit your needs. We recommend that you start small with the volume sizes, as you can't shrink them later on without deleting them, which will force you to backup/restore your data. The default values give comfortable volume sizes to start working on the platform, you can always scale them later.

## Install Ryax

Once you have customized your values you may install Ryax on your cluster :

```bash
docker run -v $PWD:/data/volume -u $UID ryaxtech/ryax-adm:latest apply --values volume/ryax_values.yaml
```

If the installation fails, check the logs, check your configuration and try again. If you are lost you join our [Discord server](https://discord.gg/ctgBtx9QwB) and we will be happy to help!

## Configure your DNS

The last step is configuring your DNS so that you can connect to your cluster. The address you should register is \<clusterName\>.\<domainName\>.ryax.io.

To retrieve the external IP of your cluster, run this one-liner
```bash
kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].ip}'
```

If you want something easier to type day to day: `kubectl -n kube-system get svc traefik`, under "External IP".

