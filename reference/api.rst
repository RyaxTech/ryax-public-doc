API
=======================

APIs can be used to create your own tools dealing with Ryax.
The frequency of the requests made are limited to prevent instances from being overloaded.


All API versions:

.. toctree::
    :glob:

    ../api/*
