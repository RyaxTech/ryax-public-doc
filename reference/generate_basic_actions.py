

import yaml

import glob
import shutil
from pathlib import Path






print("""
# Public actions and triggers

Here is the list of actions and triggers builtin Ryax.

The code source of these actions are published in an open source license and can be found [here](https://gitlab.com/ryax-tech/workflows/default-actions).

<style>

nz-card {
background-attachment: scroll;
background-clip: border-box;
background-color: rgb(255, 255, 255);
background-image: none;
background-origin: padding-box;
background-position-x: 0%;
background-position-y: 0%;
background-repeat: repeat;
background-size: auto;
border-bottom-color: rgb(240, 240, 240);
border-bottom-left-radius: 8px;
border-bottom-right-radius: 8px;
border-bottom-style: solid;
border-bottom-width: 0.8px;
border-image-outset: 0;
border-image-repeat: stretch;
border-image-slice: 100%;
border-image-source: none;
border-image-width: 1;
border-left-color: rgb(240, 240, 240);
border-left-style: solid;
border-left-width: 0.8px;
border-right-color: rgb(240, 240, 240);
border-right-style: solid;
border-right-width: 0.8px;
border-top-color: rgb(240, 240, 240);
border-top-left-radius: 8px;
border-top-right-radius: 8px;
border-top-style: solid;
border-top-width: 0.8px;
box-sizing: border-box;
color: rgb(30, 23, 53);
display: block;
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 14px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 22px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
margin-bottom: 32px;
margin-left: 0px;
margin-right: 0px;
margin-top: 0px;
overflow-x: hidden;
overflow-y: hidden;
padding-bottom: 0px;
padding-left: 0px;
padding-right: 0px;
padding-top: 0px;
position: relative;
width: 566px;
}

.ant-card-body {
align-items: center;
box-sizing: border-box;
color: rgb(30, 23, 53);
display: flex;
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 14px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 22px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
padding: 0px;
display: flex;
align-items: center;
}

.ryax-module-icon {
box-sizing: border-box;
color: rgb(30, 23, 53);
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 14px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 22px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
margin-right: 16px;
position: relative;
}

nz-card img {
box-sizing: border-box;
min-height: 72px;
vertical-align: middle;
width: 72px;
}

nz-card img::after {
box-shadow: rgb(199, 197, 205) 0px 0px 5px 0px inset;
box-sizing: border-box;
color: rgb(30, 23, 53);
content: "";
display: block;
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 14px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
height: 72px;
line-height: 22px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
pointer-events: none;
position: absolute;
top: 0px;
width: 72px;
}

.ng-star-inserted {
box-sizing: border-box;
color: rgb(30, 23, 53);
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 14px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 22px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;

}


.ryax-card-title {
box-sizing: border-box;
color: rgb(30, 23, 53);
display: inline-block;
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 18px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
font-weight: 700;
line-height: 28.2833px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
margin-bottom: 0px;
margin-left: 0px;
margin-right: 0px;
margin-top: 0px;
}


.ryax-card-version {
box-sizing: border-box;
color: rgb(165, 162, 174);
display: inline-block;
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 11px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 17.2833px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
margin-left: 16px;
}


.ryax-card-description {
box-sizing: border-box;
color: rgb(91, 104, 114);
font-family: Nunito Sans, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
font-feature-settings: "tnum", "tnum";
font-size: 13px;
font-variant-alternates: normal;
font-variant-caps: normal;
font-variant-east-asian: normal;
font-variant-ligatures: normal;
font-variant-numeric: tabular-nums;
font-variant-position: normal;
line-height: 20.4333px;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
margin-bottom: 0px;
margin-left: 0px;
margin-right: 0px;
margin-top: 0px;
text-overflow: ellipsis;
word-wrap: break-word;
overflow: hidden;
max-height: 42px;
max-width: 500px;
}

</style>
    """)


def print_action(path: str) -> None:
    for action_path in glob.glob(path):
        action = yaml.safe_load(open(action_path))
        
        logo = "../_static/action_logos/default_logo.png"
        if "logo" in action["spec"]:
            action_logo = Path(action_path).parent.joinpath(action["spec"]["logo"])
            logo = f"../_static/action_logos/{action['spec']['id']}{Path(action['spec']['logo']).suffix}"
            shutil.copy(action_logo, logo)
            
        print(f"""
<nz-card  class="ant-card ant-card-bordered">
    <div class="ant-card-body">
        <div class="ryax-module-icon">
            <img src="{logo}">
        </div>
        <div class="ng-star-inserted">
            <p class="ryax-card-title">{action["spec"]["human_name"]}</p>
            <span class="ryax-card-version">{action["spec"]["version"]}</span>
            <p class="ryax-card-description">{action["spec"]["description"]}</p>
        </div>
    </div>
</nz-card>
    """)
    

print("## Triggers")

print_action("../../default-actions/triggers/*/ryax_metadata.yaml") 


print("## Actions")


print_action("../../default-actions/actions/*/ryax_metadata.yaml") 

print("""
## And more to come…

Here’s a not exhaustive/non-guaranteed list of the public actions & triggers we are currently working on… get in touch with us for updates!
- Streamlit
- Bubble
- Waalaxy
- Hubspot
- Google Sheet


""")



