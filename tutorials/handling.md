
# A first tour of the platform

Ryax was designed to be used on a daily basis by people with various IT skill levels.
Its low-code interface enables you to deploy and run any Data Science project in a few clicks.
For expert users, advanced features and operations are covered in other section of this guide.


Through this guide, we'll use the left menu panel a lot:

<span style="width: 120px; display: inline-block;">![](media_handling/0.jpg)</span>

This panel can be collapsed to adapt to your screen and leave more room for focused editing in the main operational sections of Ryax.

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding-left: 6px; padding-right: 6px;">

![](media_handling/d62adb4a-237c-40ff-831c-188e23813273.png)

</div></td>
<td><div style="position: relative; padding-left: 6px; padding-right: 6px;">
Use the arrow at the bottom left to collapse/uncollapse the menu panel
</div></td>
</tr>
</tbody>
</table>


First of all, let's have a look at Ryax's main screens, navigating using the left menu:
the Studio,
the Executions center,
the Modules pipeline & catalog,
the Infrastructure monitoring board,
the User Management center,
and the Help menu.



## Studio

### List view

Welcome to the Ryax Studio, the heart of our platform.
This is where you will be able to create, assemble, configure, deploy and run Data Science projects with no coding skills required.

Click on the "Studio" button on the left panel to access it, a screen opens with a list of existing workflows:

![](media_handling/1.jpg)

From this view, you can:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/9b6c0777-4e60-4751-a3e6-551f53a4d7f5.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Create a new workflow
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/39fa77d2-b602-42f0-93d7-b52ea9ad6d72.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Refresh the workflow list and deployment statuses
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/2.jpg)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Access the editing canvas: clicking anywhere on the workflow's title line
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/e2271a29-ea9a-4103-8119-78e2eca62bed.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Deploy any workflow on the spot: clicking on the contextual "play" button
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/8b3e4723-12f3-4b06-bec5-fa17aaeaed64.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Undeploy a workflow, clicking on the contextual "stop" button
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/c7cd931d-4ea1-49e1-9784-8d9ad15a6707.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Launch the "Form" assistant to trigger your workflow using a user-interactive form
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/f0702ed3-c23b-4c2c-b790-08ee1407c235.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Duplicate a workflow and edit its metadata (name, description), clicking on the "..." contextual button
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/e1f55232-3fad-43b1-a0bf-fe9d4f9338cc.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Search for a specific workflow
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/1aa42f85-5f81-4e58-b881-f3ade036ce4d.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Filter the workflow list by deployment state
</div></td>
</tr>
</tbody>
</table>

The workflow list lets you read workflows' names, deployment statuses, owners, and short descriptions.

Once you click on a workflow line, you'll enter the workflow editing screen, known as the "canvas editor view" of the studio.


### Canvas editor view

The Editor view gives you access to all the controls you need to assemble, configure, edit and deploy your Data Science workflows.

It is composed of 3 main parts:
- In the center: the canvas editor, used to graphically assemble workflows.
- On top: the deployment controls.
- On the right: the contextual editing panel.

![](media_handling/3.jpg)

Using the top controls, you can:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/988e6d3b-9f70-4f33-baae-ce87d475d7d1.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Refresh the canvas and deployment statuses.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/168b0d81-f314-4e96-ae21-9c5502f7cd48.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Deploy your workflow.
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/dbb08f03-cf24-4343-8f29-c0216cde2063.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Undeploy your workflow.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/9e975191-8b9e-43ce-95cd-413ed3592a56.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Edit your workflow's metadata, duplicate it, export it, delete it.
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/d4e01bcb-18d8-41fb-9b6e-5691f4afef87.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
View your workflow's executions (contextual, workflow has to be deployed).
</div></td>
</tr>
</tbody>
</table>


Using the canvas, you can:


<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/ef7a2da2-cdef-4d99-80a7-cd89724049cc.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Drag&amp;drop modules anywhere you like, holding your left mouse button.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/d45999c6-6037-493d-9b60-45ed200ef229.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Interconnect modules together, dragging the link control (spot at the right of each module).
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/462f6b5c-d03c-4a3d-8efb-8e36b55edb60.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Access contextual module controls (click on a module for them to appear).
</div></td>
</tr>
</tbody>
</table>


To use the contextual editing panel, click the relevant controls on the
right:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/3d2555d2-8b73-486a-91db-36285092ca77.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Opens the module store pane.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/c60cae2e-be53-4ce9-a99d-43daf6ccbd30.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Opens the module setup pane (contextual, a module has to be selected in the canvas to access this section).
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/f7a8519a-79fe-4864-8ca3-e4bb6ec5bc60.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Opens the module warning and debug pane.
</div></td>
</tr>
</tbody>
</table>

The module store pane allows you to search for modules and then drag them in the canvas:

![](media_handling/4.jpg)

The module setup pane enables you to configure modules inputs, outputs and properties (see detailed configuration steps in the "configuring modules" section of this guide):

![](media_handling/5.jpg)

The warning pane enables you to observe your workflow being built and access relevant information regarding its readiness for production.
Ryax provides you natively with feedback to help you assemble a ready-to-deploy Data Science workflow:

![](media_handling/6.jpg)



## Executions

When deployed, workflows can be triggered by source events and generate executions (see the "Ryax concepts" section for more details).
Ryax allows for precise observation of these executions via the “Executions" screens, to facilitate workflow comprehension, debug and optimization.

In the context of Ryax, and as a reminder:
"Execution" means a **module run** that was triggered by the propagation of a deployed workflow upon receiving a valid triggering event.
Executions can also be understood at the workflow level: one execution meaning one *workflow run* from end to end.

For the rest of this chapter, "execution" will describe a run at the *module level*.

![](media_handling/b7456da9-910e-40bf-bc28-3146cb84dd08.png)


### List view

The menu's first screen features a list view of all module executions, latest executions on top by default.
You can change the sorting rules using the sort controls on top of the list:

<span
style="width: 350px; display: inline-block;">![](media_handling/2d9ebc93-fb39-4f56-84d6-9dd7310fe40a.png)</span>

By default, this screen lists all executions from all the modules running on your Ryax platform regardless of the workflow that carry them.
But, you can filter them out to display only those that are relevant to you.
For this, use the filter controls on top (to filter executions from a
specific workflow, module or status):

![](media_handling/e6873881-23d3-4c81-a79b-e5da8ac2d3a4.png)

Use the top refresh button to actualize the execution list with the latest data:

<span
style="width: 100px; display: inline-block;">![](media_handling/d4b805a0-e4db-45ff-a234-db7fcfc4fbef.png)</span>


Here is a quick view of the executions statuses you can encounter:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 10%" />
<col style="width: 90%" />
</colgroup>
<tbody>
<tr class="odd">
<td>

![](media_handling/status-Submitted.png)

</td>
<td>
Submitted: this module's execution has not yet started per se, it is waiting for input data to arrive (e.g. uploading data from a third party service, etc), and will then start effectively.
</td>
</tr>
<tr class="even">
<td>

![](media_handling/status-Running.png)

</td>
<td>
Running: this module's execution is being computed.
</td>
</tr>
<tr class="odd">
<td>

![](media_handling/status-Error.png)

</td>
<td>
Error: this module's execution couldn't complete because of an error. See this module's logs for more details and debugging help.
</td>
</tr>
<tr class="even">
<td>

![](media_handling/status-Done.png)

</td>
<td>
Done: this module's execution has completed successfully. The workflow execution has moved to the next module (if any).
</td>
</tr>
</tbody>
</table>

These statuses are here to help you assess the state of modules' executions for any deployed workflow.
Clicking on any execution row will lead you to this execution's details panel.


### Details view

Extracting insights from workflows and modules' executions is critical to understand their behaviors in production.
This is why Ryax provides a native execution dashboard, accessible when clicking on any execution row from the list view, or by hitting the "view executions" button from the Studio Editor screen (see the "Studio" section).

![](media_handling/44fe5225-2782-456a-b824-3b4f2e0d5c42.png)

Here is an overview of the execution dashboard:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/49c7246f-987b-4b44-9f25-b82c4350eb95.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Metadata section: displays contextual data about the execution (when, where, how).
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/d063d610-5e29-4240-bed7-5cb8fababb69.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Log visualizer: displays logs that were returned by the run module (if any).
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/e489fcf4-a6a9-4b5f-b97f-a7c2a8e78025.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Timeline chart: displays a Gant-like chart with timestamps and execution duration. Gaps in between the execution bars represent the workflow propagation delay of running from one module to the next and are mostly irrelevant (less than a few microseconds).
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/55929e6f-a1fb-45dd-bf8e-e12b3b199df4.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Previous modules outputs: used to display and access upstream modules executions' data.
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/4976bba2-9910-446e-bdb6-c770bce33b14.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Module outputs: used to display and access the current module execution' data.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/aa067b8a-1431-4965-84c1-472c0204fcb6.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Previous executions: a list of upstream modules' executions.
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/90b1858f-fb85-409a-872b-757614a85481.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Next executions: a list of downstream modules' executions.
</div></td>
</tr>
</tbody>
</table>



## Module store

As described in the "Ryax concepts" section, modules are
stateless applications (bits of code) that receive input data, do some computation, and produce output data.

There are 3 main types of modules within Ryax:

<table class="zw-table" data-node-id="node_09360389751378657" style="width: 100%; margin-left: 0in; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 22%" />
<col style="width: 77%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/711540a4-bc46-491c-a63a-739515d39e64.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">

**Sources:** to ingest data from external resources (like a database, a clock, a platform, …), also acting as workflow triggers. Always used upstream in a workflow.

</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/4fa00d80-1550-4765-a9f2-d7c1b2b57025.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">

**Processors:** to process and transform data. Often used as intermediate steps in a workflow.

</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/2306803a-2fb9-45c2-8232-a9e91d59adcb.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">

**Publishers:** to publish data to external services. Often used downstream in a workflow.

</div></td>
</tr>
</tbody>
</table>



### List view

These modules are listed in the "Store" section of the Ryax platform:

![](media_handling/7.jpg)

Modules are categorized along with their type: source, processor or publisher.
You can find the module you're searching for using the search bar at the top:

![](media_handling/22c2a736-9e42-43f1-8273-818a7a77b594.png)

Each listed module displays quick view information to help you navigate
through existing sources, processors and publishers:

<span
style="width: 300px; display: inline-block;">![](media_handling/8.jpg)</span>


### Details view

By clicking on a module, you can access detailed information and documentation:

![](media_handling/9.jpg)

These screens display the modules' full descriptions, as well and inputs and outputs details.


### \[Reminder\] Studio view

As a reminder, modules can also be accessed and directly dragged into a workflow using the studio editor (see "Studio" section of this guide).

![](media_handling/4.jpg)

## Module builder

Ryax enables you to create your own modules from code that was developed outside the platform.
For this purpose, we'll use the "Module builder" section.

### Repositories

One of the most standard ways Data Scientists (and developers in general) store, version, review and collaborate on their code is using Git repositories.

Leveraging this common practice, Ryax provides an interface able to pull code from any Git environment to create custom modules that can be used in Ryax workflows.

Let's navigate the "Repositories" section of the platform using the left menu bar:

<span
style="width: 300px; display: inline-block;">![](media_handling/24e19791-fc27-49b6-8306-eebdcb259bc7.png)</span>

This screen allows you to save and manage a list of repositories you wish to pull your code from:

![](media_handling/4f555109-8cd5-41c5-b2e6-4d8456be20c5.png)

To list a new repository, click the "New repository" button at the top right.
You'll be asked to enter a repo name and its URL:

<span style="width: 300px; display: inline-block;">![](media_handling/15f06208-f805-4154-a67b-0fb5dab3b276.png)</span>


``` warning:: Your repository URL must end with the ".git" extension for Ryax to properly interpret the repo's path.
```

After entering you repo's name and URL, you can click the "Scan repository" button to start searching for code to pull:

<span
style="width: 220px; display: inline-block;">![](media_handling/da506d05-79f4-4563-8ced-78fb379e2307.png)</span>

You'll
then be asked to enter your credentials to allow access for Ryax to
crawl your repo:

![](media_handling/93714ce6-938a-4cd6-aa0c-214c26c55d28.png)

``` warning:: Some Git repositories are configured to enable 2-factor authentication (2FA) by default. If that is the case for your repository, using your own personal credentials may not enable access to Ryax. To solve this, you'll have to create a third-party app authentication token, and use these specifically-created credentials in Ryax.
```

For example, the process for Gitlab (it may be different for your Git provider) is [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token).

Then,
you can hit the "Scan repository" button to initiate the repository scan:

<span style="width: 400px; display: inline-block;">![](media_handling/86fe5b15-439c-4bf7-a0f0-f3e6dbd665fb.png)</span>

Ryax will crawl the URL you provided and list all recognizable modules in the "Scan results" panel below:

![](media_handling/9d6a3002-e1a1-46c6-a6ef-84cc71ae23df.png)


``` note:: For more information on how to build recognizable modules for Ryax, please refer to the "How-to" section of this guide.
```

After reviewing the result list, you may luanch a module build by clicking on the lightning button to the right:

<span
style="width: 110px; display: inline-block;">![](media_handling/1bc10917-ae70-4817-a472-2ef5aacacca5.png)</span>


This "Build module" command will launch the actual code pull into Ryax.
The platform will automatically pull your code, package it in a productizable way and publish it in the Ryax store for future use in any workflow.



### Module builds

After scanning repositories, you'll be able to monitor all scanned modules in the "Module builds" section of the Ryax platform:

<span
style="width: 300px; display: inline-block;">![](media_handling/a8790f8c-bf25-4698-88e3-21f267e7ecaa.png)</span>

You'll then be able to view a list of all scanned modules, manage it, and launch module builds:

![](media_handling/1edaa292-f7f1-4bbb-8011-eb038b090491.png)

You can use the contextual buttons on the right of the list to:

<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/03dfa8a3-290e-4c63-a55a-94299c636eb7.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Build/or retry build a module.
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/78d0ccca-afe3-4a83-bb14-30d06c52e6b9.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Access a module's documentation (if built).
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_handling/124c9fc7-79e7-42a5-b28e-8291a7703ef3.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
Delete a module scan.
</div></td>
</tr>
</tbody>
</table>


In-list statuses inform you about module build states at any moment:

![](media_handling/c4be89b8-c3f7-417c-a6cf-30abc7b506d4.png)

When successfully built, modules will appear in the Ryax module store as well
as the contextual module panel of the Studio editor. You'll then be able
to drop them into a workflow.



## Help

Support inquiries can be filed using the "Help" section of the Ryax platform.

The "Support" button allows you to ask for help, a member of our team will get in touch with you with a solution within 1 working day.

<span
style="width: 300px; display: inline-block;">![](media_handling/8743fb71-9c9d-4864-a656-9ff63bfa4160.png)</span>

The "Feedback" button enables you to provide any feedback and is very
helpful for us to improve our platform to suit your needs better.

<span
style="width: 300px; display: inline-block;">![](media_handling/6eda53d1-5d80-454f-97b6-b77065459300.png)</span>

