#!/usr/bin/env bash

set -u

SPEC=$1

VERSION=${2:-$(cat $SPEC | jq -r .info.version)}

echo Version found is $VERSION

sed  "s/__RYAX_API_VERSION__/$VERSION/g" ./api_template/spec.rst > api/$VERSION.rst
cp -v $SPEC ./_static/api/$VERSION-spec.json
