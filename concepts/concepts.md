# Workflows, Actions and Runs

Ryax is more than just a tool; it is a complete framework to develop, deploy, and maintain backends. To do so, Ryax proposes a paradigm. To fully understand the framework, in this section we will go through the main concepts in Ryax: workflows, triggers, actions and runs.

![Workflow overview](../_static/concepts/overview.png "Workflow overview")

The above is a snapshot of a *workflow*. A workflow is a chain of conputational steps that can serve diverse purposes. Each of the subsequent numbered boxes are called *actions*. These are the units of computation that take some inputs (or none at all), and can produce outputs. Linking multiple actions together gives us a workflow.

**Workflows are**:

- a set of linked actions.
- without any loops; they are Directed Acyclic Graphs (DAGs).
- data streams; the outputs of any given action are accessible to every downstream action.

There are 2 types of *actions*:

- **Triggers** are the actions that ingest data from the outside world or are triggered by any internal or external event and will begin the run of a workflow. They are long-running processes responsable for triggering new workflow runs. For example, a trigger can start a new run every day at 6 pm; or every time that an email arrives in a given mailbox.
- **Actions** are stateless processes that compute *things*. They ingest data from upstream through their inputs and produce outputs which are added to the stream and can be used doenstream.

An action computing some data is called a **run**. In Ryax, a run holds everything related to it: the input data, the output data, the logs, etc.


## Creating a Ryax workflow

To create a Ryax workflow, answer these questions:

1) *What triggers the workflow?* Many events in the outside world may trigger a Ryax workflow. It can be an email, a new file in a file share, submission of an online form, a new contact in a CRM or some IoT data, for example.
2) *Who needs the results of the workflow?* What are the tools they can access? For instance, a salesperson may need this data to be attached to the company profiles in a CRM, or predictive maintenance results may need to be accessed by managers to plan actions and by workers to perform the recommended actions.
3) *What data do you need to be able to run the required computations?* Where do you gather it? In what formats do they come?
4) *Are my computations done in several steps?* Are there some steps that I can reuse in other workflows? Can I salvage actions from prior workflows?

The first question helps define triggers. The second one is for actions.
Finally, 3 and 4 help define re-usable actions.
